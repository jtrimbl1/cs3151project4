/*
 * 
 */
package edu.westga.cs3151.project4;

import java.io.File;
import java.util.Scanner;

import edu.westga.cs3151.project4.model.BinarySearchTree;
import edu.westga.cs3151.project4.model.FileReader;

/**
 * The entry point for the application.
 * 
 * @author Jeremy Trimble, CS3151 Spring 2019
 * @version 3/13/2019
 *
 */
public class Main {

	/**
	 * The main method for the application.
	 * 
	 * @param args not used
	 */
	public static void main(String[] args) {
		FileReader reader = new FileReader();

		String[] stories = {
				"C:\\Users\\Jeremy\\eclipse-workspace\\JeremyTrimbleProject4\\src\\edu\\westga\\cs3151\\project4\\stories\\The_Hound_of_the_Baskervilles.txt",
				"C:\\Users\\Jeremy\\eclipse-workspace\\JeremyTrimbleProject4\\src\\edu\\westga\\cs3151\\project4\\stories\\The_Great_Boer_War.txt",
				"C:\\Users\\Jeremy\\eclipse-workspace\\JeremyTrimbleProject4\\src\\edu\\westga\\cs3151\\project4\\stories\\The_Adventures_of_Sherlock_Holmes.txt",
				"C:\\Users\\Jeremy\\eclipse-workspace\\JeremyTrimbleProject4\\src\\edu\\westga\\cs3151\\project4\\stories\\The_Memoirs_of_Sherlock_Holmes.txt" };
		for (String current : stories) {
			BinarySearchTree<String> story = new BinarySearchTree<String>();
			File file = new File(current);
			reader.loadFilesToBinarySearchTree(story, file);
			System.out.println("Height =" + story.height() + " Size =" + story.size());
			Scanner scan = new Scanner(System.in);
			scan.nextLine();
		}
	}

}