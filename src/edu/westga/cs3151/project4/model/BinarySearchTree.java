/*
 * 
 */
package edu.westga.cs3151.project4.model;

/**
 * Extends BinaryTree to implement a classic Binary search tree ADT
 * 
 * @author Jeremy Trimble CS3151 Spring 2019
 * @version 3/2/2019
 *
 * @param <T> Generic Type
 */
public class BinarySearchTree<T extends Comparable<T>> extends BinaryTree<T> {

	/**
	 * Adds a new node to the BinarySearchTree with the specified element.
	 *
	 * @precondition element != null
	 * @postcondition size() == size()@prev + 1
	 *
	 * @param element the element for the node being added.
	 * 
	 */
	public void add(T element) {
		if (element == null) {
			throw new IllegalArgumentException("can't add null to tree");
		}
		BTNode<T> node = new BTNode<T>(element);

		if (this.getRoot() == null) {
			this.setRoot(node);
		} else {
			BTNode<T> focusNode = this.getRoot();

			while (true) {
				BTNode<T> parent = focusNode;
				if (element.compareTo(focusNode.getElement()) <= 0) {
					focusNode = focusNode.getLeft();

					if (focusNode == null) {
						parent.setLeft(node);
						return;
					}
				} else {
					focusNode = focusNode.getRight();

					if (focusNode == null) {
						parent.setRight(node);
						return;
					}
				}
			}

		}

	}

	/**
	 * Searches for a node that contains the specified element. Returns true if it
	 * is found.
	 * 
	 * @precondition element != null
	 * @postcondition none
	 * 
	 * @param element the element to search for through the tree
	 * @return the element provided as a parameter if element was found, else null
	 */
	public T search(T element) {
		if (element == null) {
			throw new IllegalArgumentException("Element cannot be null");
		}

		BTNode<T> current = this.getRoot();

		while (current.getElement().compareTo(element) != 0) {

			if (current.getElement().compareTo(element) < 0) {
				current = current.getLeft();
			} else if (current.getElement().compareTo(element) > 0) {
				current = current.getRight();
			}

			if (current == null) {
				return null;
			}
		}
		return current.getElement();
	}

	/**
	 * Removes the node with the specified element if it exist.
	 * 
	 * @precondition element != null
	 * @postcondition size() = size()@prev -1 if element exist
	 * 
	 * @param element the element of the node to be removed.
	 * @return the node that is being removed or null no node contained the element.
	 */
	public BTNode<T> remove(T element) {
		if (element == null) {
			throw new IllegalArgumentException("Element cannot be null");
		}
		BTNode<T> result = null;
		BTNode<T> parent = this.getRoot();
		BTNode<T> current = this.getRoot();

		while (current.getElement().compareTo(element) != 0) {
			parent = current;

			if (current.getElement().compareTo(element) < 0) {
				if (current.hasLeft()) {
					current = current.getLeft();
				} else {
					return null;
				}
			}

			if (current.getElement().compareTo(element) > 0) {
				if (current.hasRight()) {
					current = current.getRight();
				} else {
					return null;
				}
			}
		}

		if (!current.hasLeft() && !current.hasRight()) {
			result = current;
			current = null;
		}

		if (current.hasLeft() && current.hasRight()) {

		}
		return result;
	}
}
