/*
 * 
 */
package edu.westga.cs3151.project4.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import edu.westga.cs3151.adt.redblacktree.model.RedBlackTree;

/**
 * The FileReader Class
 * 
 * 
 * @author Jeremy Trimble, CS3151 Spring 2019
 *
 * @version 3/14/2019
 */
public class FileReader {

	/**
	 * Loads the file provided into the BinarySearchTree provided.
	 * 
	 * @precondition story != null && file != null
	 * @postcondition all text in provided file is added to the BST.
	 * 
	 * @param story the BST files are added to
	 * @param file  the file that has the story
	 */
	public void loadFilesToBinarySearchTree(BinarySearchTree<String> story, File file) {
		try (Scanner scanny = new Scanner(file)) {
			int count = 0;
			while (scanny.hasNextLine()) {
				String line = scanny.nextLine();
				String[] words = line.split("\\s+");

				for (String word : words) {
					story.add(word);
					System.out.println(count++);
				}
				System.out.println("not done");
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Loads the file provided into the RedBlackTree provided.
	 * 
	 * @precondition story != null && file != null
	 * @postcondition all text in provided file is added to the RBT.
	 * 
	 * @param story the RBT files are added to
	 * @param file  the file that has the story
	 */
	public void loadFilesToRedBlackTree(RedBlackTree<String> story, File file) {
		try (Scanner scanny = new Scanner(file)) {
			int count = 0;
			while (scanny.hasNextLine()) {
				String line = scanny.nextLine();
				String[] words = line.split("\\s+");

				for (String word : words) {
					story.add(word);
					System.out.println(count++);
				}
				System.out.println("not done");
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		}
	}
}
