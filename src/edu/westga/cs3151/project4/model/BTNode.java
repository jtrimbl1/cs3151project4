/*
 * 
 */
package edu.westga.cs3151.project4.model;

/**
 * Implements a node of type T for a binary tree
 * 
 * @author Jeremy Trimble CS3151 Spring 2019
 * @version 3/8/2019
 * 
 * @param <T> Generic Type
 */
public class BTNode<T> {

	private T element;
	private BTNode<T> left;
	private BTNode<T> right;

	/**
	 * Creates a new BTNode with the element parameter as the element of the node.
	 * BTNode's left and right children are set to null.
	 *
	 * @precondition element != null
	 * @postcondition new node is created
	 * 
	 * @param element the element
	 */
	public BTNode(T element) {
		if (element == null) {
			throw new IllegalArgumentException("Element cannot be null");
		}

		this.element = element;
		this.left = null;
		this.right = null;
	}

	/**
	 * Returns the left child node
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the left child node
	 */
	public BTNode<T> getLeft() {
		return this.left;
	}

	/**
	 * Sets the left child node to node specified.
	 * 
	 * @precondition left != null
	 * @postcondition none
	 * 
	 * 
	 * @param left the node to be set as the left child.
	 */
	public void setLeft(BTNode<T> left) {
		if (left == null) {
			throw new IllegalArgumentException("Left cannot be null");
		}
		this.left = left;
	}

	/**
	 * Gets the right child node.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the right child node.
	 */
	public BTNode<T> getRight() {
		return this.right;
	}

	/**
	 * Sets the right child node to node specified.
	 *
	 * @precondition right != null
	 * @postcondition none
	 * 
	 * @param right the new right
	 */
	public void setRight(BTNode<T> right) {
		if (right == null) {
			throw new IllegalArgumentException("Right cannot be null");
		}
		this.right = right;
	}

	/**
	 * Gets the element of the node.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the element of the node.
	 */
	public T getElement() {
		return this.element;
	}

	/**
	 * Sets the element of the node.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @param element the element being passed into the node.
	 */
	public void setElement(T element) {
		this.element = element;
	}

	/**
	 * Checks to see if the node has a left child node.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return true if the node as a left child.
	 */
	public boolean hasLeft() {
		return this.left != null;
	}

	/**
	 * Checks to see if the node has a right child node.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return true if the node as a right child.
	 */
	public boolean hasRight() {
		return this.right != null;
	}

	/**
	 * Checks if the node has neither a left or right child node.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return true if the node has no children
	 */
	public boolean isLeaf() {
		return !this.hasLeft() && !this.hasRight();
	}
}
