/*
 * 
 */
package edu.westga.cs3151.project4.model;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Implements a binary tree of type T
 * 
 * @author Jeremy Trimble CS3151 Spring 2019
 * @version 3/8/2019
 * @param <T>
 */
public class BinaryTree<T> implements Iterable<T> {

	private BTNode<T> root;

	/**
	 * Creates a new Binary Tree and sets the root node to null.
	 * 
	 * @preconditions none
	 * @postcondition none
	 * 
	 */
	public BinaryTree() {
		this.root = null;
	}

	/**
	 * Creates a new Binary Tree and sets the root to the node passed as a
	 * parameter.
	 * 
	 * @preconditions node != null
	 * @postcondition none
	 * 
	 * @param node the node to be the root of the BinaryTree.
	 */
	public BinaryTree(BTNode<T> node) {
		if (node == null) {
			throw new IllegalArgumentException("Node cannot be null");
		}
		this.root = node;
	}

	/**
	 * Creates a new Binary Tree and sets the root's element to the element passed
	 * as a parameter.
	 * 
	 * @preconditions element != null
	 * @postcondition none
	 * 
	 * @param element the element to be set for the root node of the tree.
	 */
	public BinaryTree(T element) {
		if (element == null) {
			throw new IllegalArgumentException("Element cannot be null");
		}
		this.root = new BTNode<T>(element);
	}

	/**
	 * Returns the root node of the BinaryTree
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the node at the root of the BinaryTree
	 */
	public BTNode<T> getRoot() {
		return this.root;
	}

	/**
	 * Sets the root of the BinaryTree the the node passed as a parameter
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param root the node to be set as the root
	 */
	public void setRoot(BTNode<T> root) {
		this.root = root;
	}

	/**
	 * Returns a new BinaryTree with the left child of the current tree as the root
	 * of the new one.
	 * 
	 * @precondition The root must have a node to the left.
	 * @postcondition none
	 * 
	 * @return the BinaryTree of the left subtree.
	 */
	public BinaryTree<T> getLeftSubtree() {
		if (!this.root.hasLeft()) {
			throw new IllegalArgumentException("Root does not have a left node");
		} else {
			return new BinaryTree<T>(this.root.getLeft());
		}
	}

	/**
	 * Returns a new BinaryTree with the right child of the current tree as the root
	 * of the new one.
	 * 
	 * @precondition The root must have a node to the right.
	 * @postcondition none
	 * 
	 * @return the BinaryTree of the right subtree.
	 */
	public BinaryTree<T> getRightSubtree() {
		if (!this.root.hasRight()) {
			throw new IllegalArgumentException("Root does not have a right node.");
		} else {
			return new BinaryTree<T>(this.root.getRight());
		}
	}

	/**
	 * Returns the size of the BinaryTree.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the size of the tree.
	 */
	public int size() {
		if (this.root == null) {
			return 0;
		}
		if (this.root.isLeaf()) {
			return 1;
		}
		int size = 1;
		if (this.root.hasLeft()) {
			size += this.getLeftSubtree().size();
		}
		if (this.root.hasRight()) {
			size += this.getRightSubtree().size();
		}
		return size;
	}

	/**
	 * Returns the height of the BinaryTree
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the height of the BinaryTree
	 */
	public int height() {
		if (this.root == null || this.root.isLeaf()) {
			return 0;
		}
		int left = this.root.hasLeft() ? this.getLeftSubtree().height() : 0;
		int right = this.root.hasRight() ? this.getRightSubtree().height() : 0;

		return 1 + ((left > right) ? left : right);

	}

	/**
	 * Returns a list of the elements in the BinaryTree in preorder fashion.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a list of elements in preorder.
	 */
	public List<T> preOrderTraversal() {
		List<T> list = new LinkedList<>();
		this.preOrderTraversal(list);
		return list;
	}

	private void preOrderTraversal(List<T> list) {
		list.add(this.root.getElement());

		if (this.root.hasLeft()) {
			this.getLeftSubtree().preOrderTraversal(list);
		}
		if (this.root.hasRight()) {
			this.getRightSubtree().preOrderTraversal(list);
		}

	}

	/**
	 * Returns a list of the elements in the BinaryTree in inorder fashion.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a list of elements in inorder.
	 */
	public List<T> inOrderTraversal() {
		List<T> list = new LinkedList<>();
		this.inOrderTraversal(list);
		return list;
	}

	private void inOrderTraversal(List<T> list) {
		if (this.root.hasLeft()) {
			this.inOrderTraversal(list);
		}
		list.add(this.root.getElement());

		if (this.root.hasRight()) {
			this.inOrderTraversal(list);
		}
	}

	/**
	 * Returns a list of the elements in the BinaryTree in postorder fashion.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a list of elements in postorder.
	 */
	public List<T> postOrderTraversal() {
		List<T> list = new LinkedList<>();
		this.postOrderTraversal(list);
		return list;
	}

	private void postOrderTraversal(List<T> list) {
		if (this.root.hasLeft()) {
			this.getLeftSubtree().preOrderTraversal(list);
		}
		if (this.root.hasRight()) {
			this.getRightSubtree().preOrderTraversal(list);
		}
		list.add(this.root.getElement());
	}

	/**
	 * Returns a list of the elements in the BinaryTree in levelorder fashion.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a list of elements in leveloder
	 */
	public List<T> levelOrder() {
		List<T> list = new LinkedList<T>();
		this.levelOrderTraversal(list, this.root, 0);
		return list;
	}

	private void levelOrderTraversal(List<T> list, BTNode<T> root, int level) {
		if (root == null) {
			return;
		}
		if (level == 0) {
			list.add(root.getElement());
		}
		if (level > 0) {
			this.levelOrderTraversal(list, root.getLeft(), level - 1);
			this.levelOrderTraversal(list, root.getRight(), level - 1);
		}
	}

	@Override
	public Iterator<T> iterator() {
		return this.preOrderTraversal().iterator();
	}
}
