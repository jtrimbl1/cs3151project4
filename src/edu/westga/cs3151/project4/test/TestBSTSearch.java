/*
 * 
 */
package edu.westga.cs3151.project4.test;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs3151.project4.model.BinarySearchTree;

class TestBSTSearch {

	@Test
	void testSearchElementExistRootOnly() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		Integer number = 5;
		tree.add(number);

		assertEquals(number, tree.search(5));
	}

	@Test
	void testSearchElementDoesntExistRootOnly() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		tree.add(5);

		assertEquals(null, tree.search(4));
	}

	@Test
	void testSearchElementDoesntExistMultipleNodes() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		tree.add(5);
		tree.add(6);
		tree.add(9);
		tree.add(10);
		tree.add(3);

		assertEquals(null, tree.search(4));
	}

	@Test
	void testSearchElementExistMultipleNodes() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		Integer tester = 1;
		tree.add(tester);
		tree.add(2);
		tree.add(3);
		tree.add(4);

		assertEquals(tester, tree.search(1));
	}

	@Test
	void testSearchElementExistMultipleStringNodes() {
		BinarySearchTree<String> tree = new BinarySearchTree<String>();
		tree.add("test");
		tree.add("hey");
		tree.add("work");
		tree.add("Please");

		assertEquals("hey", tree.search("hey"));
	}

}
