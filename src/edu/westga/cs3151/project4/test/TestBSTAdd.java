/*
 * 
 */
package edu.westga.cs3151.project4.test;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs3151.project4.model.BinarySearchTree;

class TestBSTAdd {

	@Test
	public void testAddSingleToEmptyBST() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		tree.add(1);

		assertEquals(1, tree.size());
	}

	@Test
	public void testAddMultipleToEmptyBST() {
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		tree.add(1);
		tree.add(2);
		tree.add(20);
		tree.add(78);
		tree.add(90);
		tree.add(9);

		assertEquals(6, tree.size());
	}
}
