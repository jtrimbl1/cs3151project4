/*
 * 
 */
package edu.westga.cs3151.project4.test;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs3151.project4.model.BTNode;

class BTNodeTestCases {

	@Test
	void testHasLeft() {
		BTNode<Integer> node = new BTNode<Integer>(4);
		BTNode<Integer> left = new BTNode<Integer>(3);
		node.setLeft(left);

		assertEquals(true, node.hasLeft());

	}

	@Test
	void testHasRight() {
		BTNode<Integer> node = new BTNode<Integer>(4);
		BTNode<Integer> right = new BTNode<Integer>(3);
		node.setRight(right);

		assertEquals(true, node.hasRight());

	}

	@Test
	void testGetRight() {
		BTNode<Integer> node = new BTNode<Integer>(4);
		BTNode<Integer> right = new BTNode<Integer>(3);
		node.setRight(right);

		assertEquals(right, node.getRight());

	}

	@Test
	void testGetLeft() {
		BTNode<Integer> node = new BTNode<Integer>(4);
		BTNode<Integer> left = new BTNode<Integer>(3);
		node.setLeft(left);

		assertEquals(left, node.getLeft());

	}

	@Test
	void testGetElement() {
		BTNode<String> node = new BTNode<String>("Tada");

		assertEquals("Tada", node.getElement());

	}

	@Test
	void testIsLeftTrue() {
		BTNode<String> node = new BTNode<String>("Tada");

		assertEquals(true, node.isLeaf());

	}

	@Test
	void testIsLeftFalse() {
		BTNode<String> node = new BTNode<String>("Tada");
		BTNode<String> left = new BTNode<String>("Tado");
		node.setLeft(left);

		assertEquals(false, node.isLeaf());

	}

}
